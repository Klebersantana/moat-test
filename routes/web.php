<?php
Auth::routes();

Route::get('/', function () { return view('auth.login'); });

Route::group(['prefix' => 'artists', 'middleware' => ['auth']], function () {
    Route::get('/', 'ArtistsController@index');
});

Route::group(['prefix' => 'albums', 'middleware' => ['auth']], function () {
    Route::get('/', 'AlbumsController@index');
    Route::get('/create', 'AlbumsController@create');
    Route::post('/store', 'AlbumsController@store');
    Route::get('/edit/{id}', 'AlbumsController@edit');
    Route::post('/update', 'AlbumsController@update');
    Route::get('/delete/{id}', 'AlbumsController@delete');
});





