<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlbumsRequest;
use App\Model\Albums;
use App\Model\Artists;
use Illuminate\Http\Request;

class AlbumsController extends Controller
{
    public function index(){
        return view('albums.index', ['albums' => Albums::paginate(10)]);
    }

    public function create(){
        $artists = (new Artists)->getArtists();
        return view('albums.create', [
            'artists' => $artists['body']
        ]);
    }

    public function store(AlbumsRequest $request){
        try {
            Albums::create($request->all());
            $request->session()->flash('success', 'Album created successfully.');
            return view('albums.index', ['albums' => Albums::paginate(10)]);
        }catch (\Exception $exception){
            $request->session()->flash('error', 'Woops, something is wrong.');
            return view('albums.index', ['albums' => Albums::paginate(10)]);
        }
    }

    public function edit($id){
        $artists = (new Artists)->getArtists();
        return view('albums.update', ['album' => Albums::find($id), 'artists' => $artists['body']]);
    }

    public function update(AlbumsRequest $request){
        try {
            $artist = Albums::find($request->id);
            $artist->name = $request->name;
            $artist->year = $request->year;
            $artist->artist_id = $request->artist_id;
            $artist->save();
            $request->session()->flash('success', 'Album updated successfully.');
            return view('albums.index', ['albums' => Albums::paginate(10)]);
        }catch (\Exception $exception){
            $request->session()->flash('error', 'Woops, something is wrong.');
            return view('albums.index', ['albums' => Albums::paginate(10)]);
        }
    }

    public function delete($id, Request $request){
        try {
            Albums::find($id)->delete();
            $request->session()->flash('success', 'Album deleted successfully.');
            return view('albums.index', ['albums' => Albums::paginate(10)]);
        }catch (\Exception $exception){
            $request->session()->flash('error', 'Woops, something is wrong.');
            return view('albums.index', ['albums' => Albums::paginate(10)]);
        }
    }
}
