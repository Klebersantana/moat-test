<?php

namespace App\Http\Controllers;

use App\Model\Artists;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ArtistsController extends Controller
{
    public function index(Request $request){
        $artists = (new Artists)->getArtists();
        return view('artists.index', [
            'artists' => $this->paginate($request, $artists['body']),
            'status' => $artists['status']
        ]);
    }

    public function paginate(Request $request, $artists): LengthAwarePaginator{
        $page = $request->page ?? 1;
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;
        return new LengthAwarePaginator(
            array_slice($artists, $offset, $perPage, true),
            count($artists),
            $perPage,
            $page,
            ['path' => $request->url(), 'query' => $request->query()]
        );
    }
}
