<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Albums extends Model
{
    protected $fillable = ['name', 'artist_id', 'year'];

    public $timestamps = false;
}
