<?php

namespace App\Model;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Artists extends Model
{
    CONST SUCCESS_STATUS = 200;
    protected $baseUrl = 'https://moat.ai/api/task';
    protected $token = 'ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ==';

    /**
     * @return array
     */
    public function getArtists(): array{
        try {
            $response = (new Client())->request('GET', $this->getBaseUrl(), [
                'headers' => [
                    'Basic' => $this->getToken()
                ]
            ]);
            return ['status' => $response->getStatusCode(), 'body' => $this->convertArray(json_decode($response->getBody()->getContents()))];
        }catch (GuzzleException $exception){
            return ['status' => $exception->getCode(), 'body' => []];
        }catch (\Exception $exception){
            return ['status' => $exception->getCode(), 'body' => []];
        }
    }

    /**
     * @param int $id
     * @return string
     */
    public function getArtist(int $id): string{
        $artists = $this->getArtists();
        $artist = array_where($this->convertArray($artists['body']), function ($value) use ($id) {
            return $value->id == $id;
        });

        return array_first($artist)->name;
    }

    /**
     * @param array $artists
     * @return array
     */
    public function convertArray(array $artists): array{
        return array_values(array_dot($artists));
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
