@extends('layouts.app-login')

@section('content')
    <div class="wrapper">
        <div class="card">
            <form method="POST" action="{{ route('login') }}" class="d-flex flex-column">
                @csrf
                <img src="{{ asset('img/music.png') }}"
                     class="rounded mx-auto d-block w-25 mb-4" alt="moat_music">
                @if ($errors->has('username') || $errors->has('password'))
                    <span class="text-danger danger-feedback">
                        <strong>{{ $errors->has('username') ? $errors->first('username') : $errors->first('password')}}</strong>
                    </span>
                @endif
                <div class="align-items-center input-field my-3 mb-4">
                    <input type="text" class="form-control text-center"
                           id="username" name="username" placeholder="Username">
                </div>
                <div class="align-items-center input-field mb-4">
                    <input type="password" class="form-control text-center" id="password" name="password" placeholder="Password">
                </div>
                <div class="d-flex align-items-center input-field my-3">
                    <button type="submit" value="Login" class="btn btn-primary form-control text-center">
                        Login
                    </button>
                </div>
                <div class="mb-3 text-center">
                    <span class="text-light-white">Don't have an account?</span>
                    <a href="{{ route('register') }}">Sign Up</a>
                </div>
            </form>
        </div>
    </div>
@endsection
