@extends('layouts.app-login')

@section('content')
    <div class="wrapper">
        <div class="card">
            <img src="{{ asset('img/music.png') }}"
                 class="rounded mx-auto d-block w-25 mb-4" alt="moat_music">
            <form method="POST" action="{{ route('register') }}" class="d-flex flex-column">
                @csrf
                <div class="align-items-center input-field my-3 mb-4">
                    <input type="text" class="form-control text-center {{ $errors->has('full_name') ? ' is-invalid' : '' }}"
                           id="full_name" name="full_name" placeholder="Full name">
                    @if ($errors->has('full_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('full_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="align-items-center input-field mb-4">
                    <input type="text" class="form-control text-center {{ $errors->has('username') ? ' is-invalid' : '' }}"
                           id="username" name="username" placeholder="Username">
                    @if ($errors->has('username'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="align-items-center input-field mb-4">
                    <input type="password" class="form-control text-center {{ $errors->has('password') ? ' is-invalid' : '' }}"
                           id="password" name="password" placeholder="Password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="align-items-center input-field mb-4">
                    <input type="password" class="form-control text-center" id="password_confirmation"
                           name="password_confirmation" placeholder="Confirm your password">
                </div>
                <div class="form-check text-center {{ $errors->has('role') ? ' is-invalid' : '' }}">
                    <input class="form-check-input" type="radio" name="role" value="0" checked>
                    <label class="form-check-label space-between-radios">
                        User
                    </label>
                    <input class="form-check-input" type="radio" name="role" value="1">
                    <label class="form-check-label">
                        Admin
                    </label>
                    @if ($errors->has('role'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="align-items-center input-field my-3">
                    <button type="submit" value="Register" class="btn btn-primary form-control text-center">
                        Register
                    </button>
                </div>
                <div class="mb-3 text-center">
                    <span class="text-light-white">Already have an account?</span>
                    <a href="{{ route('login') }}">Login</a>
                </div>
            </form>
        </div>
    </div>
@endsection
