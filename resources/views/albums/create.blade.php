@extends('layouts.app')

@section('content')
    <div class="container-fluid my-4">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ url('albums/store') }}">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-9">
                            <label for="name" class="form-label">Album name</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-3">
                            <label for="year" class="form-label">Year</label>
                            <input type="text" class="form-control {{ $errors->has('year') ? ' is-invalid' : '' }}"
                                   id="year" name="year">
                            @if ($errors->has('year'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('year') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="artist_id" class="form-label">Artists</label>
                        <select class="form-select {{ $errors->has('year') ? ' is-invalid' : '' }}"
                                name="artist_id" id="artist_id">
                            <option value="" selected>Select an artist</option>
                            @foreach($artists as $artist)
                                <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('artist_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('artist_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="text-right">
                        <a href="{{ url('/albums') }}" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
