@extends('layouts.app')

@section('content')
    <div class="container-fluid my-4">
        <div class="card">
            <div class="card-body">
                @include('layouts.flash-messages')
                <a type="button" class="btn btn-primary my-3 mb-3" href="{{ url('albums/create') }}">Add album</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Artist</th>
                            <th scope="col">Year</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($albums as $album)
                            <tr>
                                <th>{{ $album->id }}</th>
                                <td>{{ $album->name }}</td>
                                <td>{{ (new \App\Model\Artists())->getArtist($album->artist_id) }}</td>
                                <td>{{ $album->year }}</td>
                                <td>
                                    <a href="{{ url("albums/edit/{$album->id}") }}" class="btn-outline-primary">Edit</a>
                                    @if(Auth::user()->role == \App\Model\User::IS_ADMIN)
                                        <a href="{{ url("albums/delete/{$album->id}") }}" class="btn-outline-danger">Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div>
                    {{ $albums->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
