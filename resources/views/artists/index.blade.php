@extends('layouts.app')

@section('content')
    <div class="container-fluid my-4">
        <div class="card">
            <div class="card-body">
                @if($status != \App\Model\Artists::SUCCESS_STATUS)
                    <div class="alert alert-warning" role="alert">
                        We had a problem, please try again in few minutes.
                    </div>
                @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Twitter</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($artists as $artist)
                            <tr>
                                <th>{{ $artist->id }}</th>
                                <td>{{ $artist->name }}</td>
                                <td>
                                    <a target="_blank" href="{{'https://www.instagram.com/'.substr($artist->twitter, 1)}}">
                                        {{ $artist->twitter }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div>
                    {{ $artists->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
