## How to run
1. Clone this repo
2. Run composer install for install dependencies 
3. Generate a project key (php artisan key:generate)
4. Run project migrations (php artisan migrate)
